<!DOCTYPE html>
<html>
<head>
	<title>Wojciech Guderski, Programowanie Internetowe</title>
	<meta charset="utf-8" />
	<meta name="description" content="Strona główna, Programowanie Internetowe" />
	<meta name="author" content="Wojciech Guderski" />
	<meta name="keywords" content="XHTML, programowanie, internetowe, CV" />
	<link rel="stylesheet" type="text/css" href="../Style.css" />
</head>
<body>
	<header>
		<h1>Strona główna</h1>
	</header>
	<nav>
	<h2>Linki</h2>
		<ul>
			<li><a href="index.php">Strona główna</a></li>
			<li><a href="CV/div_xhtml.html">Div xhtml</a></li>
			<li><a href="CV/table_xhtml.html">Tabela xhtml</a></li>
			<li><a href="CV/div_html5.html">Div html5</a></li>
			<li><a href="CV/table_html5.html">Tabela html5</a></li>
            <li><a href="JS/table_generation.html">Tabela JavaScript</a></li>
            <li><a href="JS/form.html">Obsługa formularzy</a></li>
            <li><a href="php/login.php">Logowanie php</a></li>
            <li><a href="php2/upload.php">Upload plików</a></li>
		</ul>
	</nav>
	<section>
    <form action="upload.php" method="post" enctype="multipart/form-data">
        Wybierz plik, który chcesz umieścić na serwerze:
        <input type="file" name="fileToUpload" id="fileToUpload">
        <input type="submit" value="Upload Image" name="submit">
    </form>
	</section>
	<footer>
		<p>
			<a href="http://jigsaw.w3.org/css-validator/check/referer">
				<img style="border:0;width:88px;height:31px"
					src="http://jigsaw.w3.org/css-validator/images/vcss-blue"
					alt="Valid CSS!" />
			</a>
		</p>
        <p><a href="https://bitbucket.org/Liffes/programowanie-internetowe/src/11f8c10946bd/WWW/">Source</a></p>
		<p>Wojciech Guderski 2015</p>
	</footer>
</body>
</html>
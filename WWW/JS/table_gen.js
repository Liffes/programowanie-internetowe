﻿function genRandFirst() {
    var firstNames = new Array("Henryk", "Jan", "Kamil", "Jakub", "Andrzej", "Stanisław", "Jacek", "Anna");
    return firstNames[Math.floor(Math.random() * firstNames.length)];
}

function genRandLast() {
    var lastNames = new Array("Kowalsky", "Kaczyński", "Iksiński", "Gwieździński");
    return lastNames[Math.floor(Math.random() * lastNames.length)];
}


var columnsAmount = 3;

//generate table
function addTable() {

    var amount = document.formularz.stud_amount.value;
    var myTableDiv = document.getElementById("tabela");

    var table = document.createElement('TABLE');
    table.setAttribute("id", "generatedTable");
    table.border = '1';

    var button = document.createElement("input");
    button.setAttribute("type", "button");
    button.setAttribute("value", "Dodaj");
    button.setAttribute("name", "button " + columnsAmount);
    button.setAttribute("onclick", "addColumn('generatedTable')");

    //create thead
    var header = new Array("Lp", "Imię", "Nazwisko");
    var tableHead = document.createElement('THEAD')
    table.appendChild(tableHead);
    var tr = document.createElement('TR');
    tableHead.appendChild(tr);
    for (var i = 0; i < header.length; i++) {
        var td = document.createElement('TH');
        td.width = '75';
        td.appendChild(document.createTextNode(header[i]));
        tr.appendChild(td);
    }
    var td = document.createElement('TH');
    td.width = '75';
    td.appendChild(button);
    tr.appendChild(td);

    //create tbody
    var tableBody = document.createElement('TBODY');
    table.appendChild(tableBody);

    for (var i = 0; i < amount; i++) {
        var tr = document.createElement('TR');
        tableBody.appendChild(tr);

        fillRow(tr, i);
    }
    myTableDiv.appendChild(table);
    removeForm();

}

function fillRow(tr, i) {

    //create cell with id
    var td = document.createElement('TD');
    td.width = '75';
    td.appendChild(document.createTextNode("" + i));
    tr.appendChild(td);

    //create cell with first name
    var td = document.createElement('TD');
    td.width = '75';
    td.appendChild(document.createTextNode(genRandFirst()));
    tr.appendChild(td);

    //create cell with last name
    var td = document.createElement('TD');
    td.width = '75';
    td.appendChild(document.createTextNode(genRandLast()));
    tr.appendChild(td);

    //create cell with the input field
    var td = document.createElement('TD');
    var inputField = document.createElement('input');
    inputField.setAttribute("type", "text");
    inputField.setAttribute("value", "");
    inputField.setAttribute("name", "input " + i + " " + columnsAmount);
    td.width = '75';
    td.appendChild(inputField);
    tr.appendChild(td);
}

function removeForm() {
    var toRemove = document.getElementsByName('formularz')[0];
    var parent = toRemove.parentNode;
    parent.removeChild(toRemove);

    var text = document.getElementById("formIntro");
    text.innerText = "Twoja tabela:";
}

function addColumn(tableId) {
    columnsAmount++;

    var tableHead = document.getElementById(tableId).tHead;
    for (var h = 0; h < tableHead.rows.length; h++) {
        var newTH = document.createElement('th');
        tableHead.rows[h].appendChild(newTH);
        var button = document.createElement("input");
        button.setAttribute("type", "button");
        button.setAttribute("value", "Dodaj");
        button.setAttribute("name", "button " + columnsAmount);
        button.setAttribute("onclick", "addColumn('generatedTable')");
        newTH.appendChild(button);
    }

    var buttonName = "button " + (columnsAmount - 1);
    var lastButton = document.getElementsByName(buttonName)[0];
    var parent = lastButton.parentNode;
    parent.removeChild(lastButton);
    var inputField = document.createElement('input');
    inputField.setAttribute("type", "text");
    inputField.setAttribute("value", "");
    inputField.setAttribute("name", "input " + i + " " + columnsAmount);
    parent.appendChild(inputField);

    var tableBody = document.getElementById(tableId).tBodies[0];
    for (var i = 0; i < tableBody.rows.length; i++) {
        var newCell = tableBody.rows[i].insertCell(-1);
        var inputField = document.createElement('input');
        inputField.setAttribute("type", "text");
        inputField.setAttribute("value", "");
        inputField.setAttribute("name", "input " + i + " " + columnsAmount);
        newCell.appendChild(inputField);
    }
}
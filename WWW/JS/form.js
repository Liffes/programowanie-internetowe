﻿function nameCheck(e) {
    e = e.wchich || event.keyCode;
    return (e <= 46 || e >= 58);
}
function numCheck(e, textBox) {
    e = e.wchich || event.keyCode;
    return (e >= 48 && e < 58);
}
function letterSize() {
    var first = document.form.firstname.value;
    var last = document.form.lastname.value;
    var znaki = "ABCDEFGHIJKLŁMNOPQRSTUVWXYZ";
    var z = znaki.split("");
    var im = first.split("");
    var naz = last.split("");

    for (var i = 0; i < 26; i++) {
        if (im[0] === z[i]) {
            for (var j = 0; j < 26; j++) {
                if (naz[0] === z[j]) {
                    return true;
                }
            }
        }
    }
    alert("Imię i nazwisko muszą rozpoczynać się z Wielkich Liter!");
    return false;
}

function ageCheck() {
    var v1 = document.form.bday.value;
    var v2 = v1.split("-");
    var v3 = v2[0];
    var a = 2015 - v3;
    document.getElementById("age").innerText ="Wiek: " + a;
}

var peselSize = 0;
var lastPeselValue;

function maxPeselSize(length) {
   
    if (document.form.pesel.value.length < length) {
        lastPeselValue = document.form.pesel.value;
    } else {
        document.form.pesel.value = lastPeselValue;
    }
    peselSize = length - document.form.pesel.value.length;
}

var lastComVal;
function maxComSize(length) {
    if (document.form.commentary.value.length > length) {
        document.form.commentary.value = lastComVal;
    } else {
        lastComVal = document.form.commentary.value;
    }
    document.getElementById("counter").innerText = "Ilość pozostałych znaków: " + (length - document.form.commentary.value.length);
}

function check(formularz) {


    if (document.form.agr.checked === false) {
        alert("Musisz wyrazić zgodę na przetwarzanie danych osobowych!");
        return false;
    }
    if (checkPesel() === false) {
            alert("Wprowadź poprawny PESEL!");
            return false;
   }
    
    for (var i = 0; i < formularz.length; i++) {
        var field = formularz.elements[i];
        if (field.type === "text" && field.value === "") {
            alert("Wypełnij wszystkie pola!");
            return false;
        }
    }
    var plik = checkFile(document.form.file.value);

    if (letterSize() != true)
        return false;

    if (plik === false) {
        return false;
    }

    return true;
}
function checkPesel() {
    var v1 = document.form.bday.value;
    var v2 = v1.split("");
    var pesel = document.form.pesel.value;
    var p = pesel.split("");
    if (v2[2] === p[0] && v2[3] === p[1] && v2[5] === p[2] && v2[6] === p[3] && v2[8] === p[4] && v2[9] === p[5]) {
        return true;
    } else {
        alert("PESEL nie zgadza się z datą urodzenia");
        return false;
    }
}
function checkFile(filename) {
    if (filename === null || filename === "")
        return true;
    var parts = filename.split('.');
    var ext = parts[parts.length - 1];
    var a = ext.toLowerCase();
    if (a === 'jpg' || a === 'png' || a === 'tif') {
        return true;
    } else {
        alert("Błędny format pliku!");
        return false;
    }
}
﻿var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
var labelIndex = 0;
var map;
var markers = [];
var route;
var directionsService;
var directionsDisplay;
var displayedOnce = false;
var start;
var end;

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: 52.23, lng: 21.016 },
        zoom: 6
    });


    google.maps.event.addListener(map, 'click', function (event) {
        addMarker(event.latLng, map);
    });

    document.getElementById('travelMode').addEventListener('change', function () {
        display();
    });


    directionsDisplay.addListener('directions_changed', function () {
        computeTotalDistance(directionsDisplay.getDirections());
    });


}

function addMarker(location, map) {
    // Add the marker at the clicked location, and add the next-available label
    // from the array of alphabetical characters.
    var marker = new google.maps.Marker({
        position: location,
        label: labels[labelIndex++ % labels.length],
        draggable: true,
        map: map
    });
    markers.push(marker);
}

function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}

function clearMarkers() {
    setMapOnAll(null);
}


function deleteMarkers() {
    clearMarkers();
    markers = [];
    labelIndex = 0;
}

function display() {
    if (markers.length > 1) {
        start = markers[0].position;
        end = markers[1].position;
    } /*else {
        start = directionsDisplay.destination.position.latLng;
        end = directionsDisplay.origin.position.latLng;
    }*/


    if (displayedOnce) {
        clearRoute();

    }
        directionsService = new google.maps.DirectionsService;
        directionsDisplay = new google.maps.DirectionsRenderer({
            draggable: true,
            map: map,
            panel: document.getElementById('right-panel')
        });
        var mode = getTravelMode();
        displayRoute(start, end, mode, directionsService, directionsDisplay);
        displayedOnce = true;
        deleteMarkers();
}

function getTravelMode() {
    var select = document.getElementById('travelMode');
    if (select.value == "Samochód")
        return google.maps.TravelMode.DRIVING;
    if (select.value == "Pieszo")
        return google.maps.TravelMode.WALKING;
    if (select.value == "Rower")
        return google.maps.TravelMode.BICYCLING;
    if (select.value == "Komunikacja")
        return google.maps.TravelMode.TRANSIT;
}

function displayRoute(origin, destination, mode, service, display) {
    route = service.route({
        origin: origin,
        destination: destination,
        travelMode: mode,
        avoidTolls: true
    }, function (response, status) {
        if (status === google.maps.DirectionsStatus.OK) {
            display.setDirections(response);
        } else {
            alert('Could not display directions due to: ' + status);
        }
    });
}


function computeTotalDistance(result) {
    directionsDisplay.travelMode = getTravelMode();
    var total = 0;
    var myroute = result.routes[0];
    for (var i = 0; i < myroute.legs.length; i++) {
        total += myroute.legs[i].distance.value;
    }
    total = total / 1000;
    document.getElementById('total').innerHTML = total + ' km';
}

function clearRoute() {
    directionsDisplay.setMap(null);
    directionsDisplay.setPanel(null);
}

function calculateAndDisplayRoute(directionsService, directionsDisplay) {
    var selectedMode = getTravelMode();
    directionsService.route({
        origin: directionsService.origin,
        destination: directionsService.destination,
        travelMode: selectedMode
    }, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            result = response;
            directionsDisplay.setDirections(response);
        } else {
            window.alert('Directions request failed due to ' + status);
        }
    });
}


<!DOCTYPE html>
<?php
session_start();
require_once("checkSiteVersion.php");
if(empty($_SESSION['logged'])){
	header('Location: login.php?action=not_logged_in');
}
?>
<html>
<head>
    <script src="../JS/form.js"></script>
    <title>Wojciech Guderski, Programowanie Internetowe</title>
    <meta charset="utf-8" />
    <meta name="description" content="Obsługa formularza za pomocą JavaScript" />
    <meta name="author" content="Wojciech Guderski" />
    <meta name="keywords" content="JavaScript, Programowanie, Internetowe, tabela, generacja" />
    <link rel="stylesheet" type="text/css" href="mobile.css" />
</head>
<body>
    <header>
        <h1>Wersja mobilna</h1>
    </header>
    <nav>
        <table>
            <tr>
                <td>
                    <a href="../index.php">Strona główna</a>
                </td>
                <td>
                    <a href="logout.php">Logout</a>
                </td>
            </tr>
        </table>
    </nav>
    <section>

        <div>
            <h3>Formularz:</h3>
            <form action="saveToSession.php" method="GET" name="form" enctype="text/plain" onsubmit="if (!check(this)) return false">
                <div>
                    <p>* - pola obowiązkowe</p>
                </div>
                <div>
                    Podaj swoje imię*:
                    <br />
                    <input type="text" maxlength="20" name="name" onkeypress="return nameCheck(event);" />
                </div>

                <div>
                    Podaj swoje nazwisko*:
                    <br />
                    <input type="text" name="surname" onkeypress="return nameCheck(event);" />
                    <br />
                </div>
                <div>
                    Wprowadź swoją datę urodzenia*:
                    <br />
                    <input onchange="ageCheck()" type="date" name="bday" />
                    <br />
                    <br />
                    <p id="age">Wiek: </p>
                </div>
                <div>
                    Podaj swój PESEL*:
                    <br />
                    <input type="text" name="pesel" onkeypress="maxPeselSize(11); return numCheck(event, this);" />

                </div>
                <div>
                    Płeć:
                    <br />
                    <select name="plec">
                        <option />
                        Kobieta
                        <option />
                        Męczyzna
                    </select>
                </div>
                <div>
                    Wybierz swój kierunek studiów*:
                    <br />
                    <input type="radio" name="studies" value="inf" checked="checked" />
                    Informatyka
                    <br />
                    <input type="radio" name="studies" value="air" />
                    Automatyka i robotyka
                    <br />
                    <input type="radio" name="studies" value="et" />
                    Elektrotechnika
                    <br />
                </div>

                <div>
                    Wpisz swój komentarz*:
                    <br />
                    <textarea name="commentary" cols="40" rows="5" onkeyup="javascript:maxComSize(200)" onkeypress="maxComSize(200)"></textarea>
                    <p id="counter">Ilość pozostałych znaków: 200 </p>
                </div>
                <div>
                    Dodaj jakiś obrazek (.jpg, .tif, .png)
                    <br />
                    <input type="file" id="file" name="file" accept=".jpg,.tif,.png" />
                    <br />
                    <br />
                </div>
                <div>
                    Czy zgadzasz się na przetwarzanie danych*?
                    <br />
                    <input type="checkbox" name="agr" value="tak" checked="" />
                    Wyrażam zgodę
                </div>
                <div>
                    <button type="submit" name="submit">Wyślij</button>
                </div>
            </form>
        </div>
        <div>
            <?
	   echo "<h2> Podane przez ciebie dane to:</h2>";
	   echo "<br>";
	   echo "Imie: ". $_SESSION['name'];
	   echo "<br>";
	   echo "Nazwisko: ". $_SESSION['surname'];
	   echo "<br>";
	   echo "Pesel: ". $_SESSION['pesel'];
	   echo "<br>";
	   echo "Plec: ". $_SESSION['sex'];
	   echo "<br>";
	   echo "Studia: ". $_SESSION['studies'];
	   echo "<br>";
	   echo "Tresc komentarza: ". $_SESSION['comment'];

            ?>
        </div>
        <div id="following">
            <h4>Programowanie internetowe</h4>
        </div>
    </section>
    <footer>
        <p>
            <a href="http://jigsaw.w3.org/css-validator/check/referer">
                <img style="border:0;width:88px;height:31px"
                    src="http://jigsaw.w3.org/css-validator/images/vcss-blue"
                    alt="Valid CSS!" />
            </a>
            <a href="http://validator.w3.org/check?uri=referer">
                <img src="http://www.w3.org/html/logo/badge/html5-badge-h-css3-semantics.png" width="165" height="64" alt="HTML5 Powered with CSS3 / Styling, and Semantics" title="HTML5 Powered with CSS3 / Styling, and Semantics" />
            </a>
        </p>
        <p>
            <a href="https://bitbucket.org/Liffes/programowanie-internetowe/src/11f8c10946bd/WWW/">Source</a>
        </p>
        <p>Wojciech Guderski 2015</p>
    </footer>
</body>
</html>

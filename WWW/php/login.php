﻿<!DOCTYPE html>
<? 
   session_start();
   if($_SESSION['logged']==true){
    header('Location: desktop.php');
    }
?>
<html>
<head>
    <title>Wojciech Guderski, Programowanie Internetowe</title>
    <meta charset="utf-8" />
    <meta name="description" content="Logowanie w php" />
    <meta name="author" content="Wojciech Guderski" />
    <meta name="keywords" content="XHTML, programowanie, internetowe, CV" />
    <link rel="stylesheet" type="text/css" href="../Style.css" />
</head>
<body>
    <header>
        <h1>Strona główna</h1>
    </header>
    <nav>
        <ul>
            <li><a href="../index.php">Strona główna</a></li>
            <li><a href="../CV/div_xhtml.html">Div xhtml</a></li>
            <li><a href="../CV/table_xhtml.html">Tabela xhtml</a></li>
            <li><a href="../CV/div_html5.html">Div html5</a></li>
            <li><a href="../CV/table_html5.html">Tabela html5</a></li>
            <li><a href="../JS/table_generation.html">Tabela JavaScript</a></li>
            <li><a href="../JS/form.html">Obsługa formularzy</a></li>
            <li><a href="login.php">Logowanie php</a></li>
        </ul>
    </nav>
    <section>
        <h2>Zaloguj się: </h2>
        <div>
            <?php
            if($_GET['action']=='not_logged_in'){
                echo 'Zaloguj się aby uzyskać dostęp';    
            }

            if (isset($_POST['login']) && !empty($_POST['username']) && !empty($_POST['password'])) {

                if ($_POST['username'] == 'student' && $_POST['password'] == 'zet') {
                    $_SESSION['logged'] = true;
                    $_SESSION['timeout'] = time();
                    $_SESSION['username'] = 'student';
                    header('Location: checkSiteVersion.php'); 
                   
                } else{
                    echo 'Błędne dane logowania';
                }
            }
            ?>
        </div>
        <div>
            <form action="login.php" method="post">
                <input type="text" name="username" placeholder="student" required autofocus>
                <br />
                <input type="password" name="password" placeholder="zet" required>
                <button type="submit" name="login">Login</button>
            </form>
        </div>
    </section>
    <footer>
        <p>
            <a href="http://jigsaw.w3.org/css-validator/check/referer">
                <img style="border:0;width:88px;height:31px"
                     src="http://jigsaw.w3.org/css-validator/images/vcss-blue"
                     alt="Valid CSS!" />
            </a>
            <a href="http://validator.w3.org/check?uri=referer">
                <img src="http://www.w3.org/html/logo/badge/html5-badge-h-css3-semantics.png" width="165" height="64" alt="HTML5 Powered with CSS3 / Styling, and Semantics" title="HTML5 Powered with CSS3 / Styling, and Semantics">
            </a>
        </p>
        <p>Wojciech Guderski 2015</p>
        <p><a href="https://bitbucket.org/Liffes/programowanie-internetowe/src/11f8c10946bd/WWW/">Source</a></p>
    </footer>
</body>
</html>